<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="springForm"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>

<jsp:include page="/WEB-INF/views/template/template1.jsp"></jsp:include>

<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">
		<h1>Fazendas</h1> 
		<security:authorize access="hasRole('ADMIN')">
								<a class="btn btn-primary pull-right" href="/curso-mvc/fazenda/cadastrar">Adicionar</a>
		</security:authorize>		
	</div>
	<div class="panel-body">
		<form action="/curso-mvc/fazenda/pesquisar">
			<input type="text" name="filter"> <button type="submit">Pesquisar</button>
		</form>
	</div>

	<table class="table">
		<tr>
			<th>Nome</th>
			<th>Proprietário</th>
			<th>Data de Fundação</th>
			<th>&nbsp;</th>
		</tr>
		<c:forEach items="${fazendas}" var="fazenda">
			<tr>
				<td>${fazenda.nome}</td>
				<td>${fazenda.proprietario}</td>
				<td>
					<fmt:formatDate value="${fazenda.dataFundacao.time}" var="dateString" pattern="dd/MM/yyyy" />
					<c:out value="${dateString }"></c:out>
				</td>
				<td><a class="btn btn-warning"
					href="/curso-mvc/fazenda/editar/${fazenda.id}"> <span
						class="glyphicon glyphicon-edit" aria-hidden="true"></span>
				</a> <a class="btn btn-danger"
					href="/curso-mvc/fazenda/apagar/${fazenda.id}"> <span
						class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</a></td>
			</tr>
		</c:forEach>
	</table>
</div>

<div class="text-center">
	<nav>
		<ul class="pagination">
			<li><a href="#" aria-label="Previous"> <span
					aria-hidden="true">&laquo;</span>
			</a></li>
			<c:forEach begin="0" end="${1 + quantidade/5 }" var="pagina">
				<li><a href="/curso-mvc/fazenda/listar?pagina=${pagina }">${pagina }</a></li>
			</c:forEach>
			<li><a href="#" aria-label="Next"> <span aria-hidden="true">&raquo;</span>
			</a></li>
		</ul>
	</nav>
</div>

<jsp:include page="/WEB-INF/views/template/template2.jsp"></jsp:include>
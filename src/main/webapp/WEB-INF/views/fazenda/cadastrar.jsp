<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="springForm"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>


<jsp:include page="/WEB-INF/views/template/template1.jsp"></jsp:include>


<div class="panel panel-default">
	<div class="panel-heading">
		<h1>Registrar</h1>
	</div>
	<div class="panel-body">
			
		<form class="form-horizontal" action="/curso-mvc/fazenda/salvar"
			method="post">
			
		<security:csrfInput/>

			<div class="form-group">
				<label for="nome" class="col-sm-2 control-label">Nome da
					Fazenda</label>
				<div class="col-sm-10">
					<springForm:input path="fazenda.nome" cssClass="form-control" />
					<springForm:errors path="fazenda.nome" cssClass="error" />
				</div>
			</div>

			<div class="form-group">
				<label for="proprietario" class="col-sm-2 control-label">Proprietário</label>
				<div class="col-sm-10">
					<springForm:input path="fazenda.proprietario" cssClass="form-control" />
					<springForm:errors path="fazenda.proprietario" cssClass="error" />
				</div>
			</div>

			<div class="form-group">
				<label for="dataFundacao" class="col-sm-2 control-label">Data
					de fundação</label>
				<div class="col-sm-10">
					<springForm:input path="fazenda.dataFundacao" cssClass="form-control" />
					<springForm:errors path="fazenda.dataFundacao" cssClass="error" />
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">Salvar</button>
				</div>
			</div>
		</form>
	</div>

</div>




<jsp:include page="/WEB-INF/views/template/template2.jsp"></jsp:include>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:include page="/WEB-INF/views/template/template1.jsp"></jsp:include>

<h1>Animais</h1>
<table class="table">
	<tr>
		<th>Nome</th>
		<th>G�nero</th>
		<th>Idade</th>
		<th>Peso</th>
		<th>&nbsp;</th>
		<th>&nbsp;</th>
	</tr>
	<c:forEach items="${animais}" var="animal">
		<tr>
			<td>${animal.nome}</td>
			<td>${animal.genero}</td>
			<td>${animal.idade}</td>
			<td>${animal.peso}</td>
			<td><a href="/curso-mvc/animal/editar/${animal.id}">Editar</a></td>
			<td><a href="/curso-mvc/animal/apagar/${animal.id}">Apagar</a></td>
		</tr>
	</c:forEach>
</table>

<br />
<a class="btn btn-primary" href="/curso-mvc/animal/cadastrar">Cadastrar Animal</a>
<jsp:include page="/WEB-INF/views/template/template2.jsp"></jsp:include>
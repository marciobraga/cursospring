<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="springForm"%>


<jsp:include page="/WEB-INF/views/template/template1.jsp"></jsp:include>

<h1>Registrar</h1>
<form class="form-horizontal" action="/curso-mvc/lote/salvar"
	method="post">

	<div class="form-group">
		<label for="nome" class="col-sm-2 control-label">Fazenda</label>
		<div class="col-sm-10">
			<select name="fazenda.id" class="form-control">
				<c:forEach items="${fazendas}" var="fazenda">
					<option value="${fazenda.id}">${fazenda.nome}</option>
				</c:forEach>
			</select>

		</div>
	</div>

	<div class="form-group">
		<label for="area" class="col-sm-2 control-label">�rea
			( m�)</label>
		<div class="col-sm-10">
			<springForm:input  path="lote.area" class="form-control" />
			<springForm:errors path="lote.area" cssClass="error" />
		</div>
	</div>

	<div class="form-group">
		<label for="inicio" class="col-sm-2 control-label">Inicio
			da Utiliza��o</label>
		<div class="col-sm-10">
			<springForm:input path="lote.inicio" class="form-control" />
			<springForm:errors path="lote.inicio" cssClass="error" />
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-default">Salvar</button>
		</div>
	</div>
</form>

<jsp:include page="/WEB-INF/views/template/template2.jsp"></jsp:include>
<jsp:include page="/WEB-INF/views/template/template1.jsp"></jsp:include>

<h1>Editar</h1>
<form class="form-horizontal" action="/curso-mvc/lote/atualizar"
	method="post">

	<input type="hidden" name="id" value="${fazenda.id}">

	<div class="form-group">
		<label for="nome" class="col-sm-2 control-label">Nome da
			Fazenda</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="fazenda.id"
				value="${lote.fazenda.id}">
		</div>
	</div>

	<div class="form-group">
		<label for="proprietario" class="col-sm-2 control-label">�rea (m�)</label>
		<div class="col-sm-10">
			<input type="number" class="form-control" name="area"
				value="${lote.area}">
		</div>
	</div>

	<div class="form-group">
		<label for="dataFundacao" class="col-sm-2 control-label">Inicio da Utiliza��o</label>
		<div class="col-sm-10">
			<input type="date" class="form-control" name="inicio"
				value="${lote.inicio}">
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-default">Atualizar</button>
		</div>
	</div>
</form>

<jsp:include page="/WEB-INF/views/template/template2.jsp"></jsp:include>
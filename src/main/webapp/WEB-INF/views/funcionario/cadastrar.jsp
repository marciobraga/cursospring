
<jsp:include page="/WEB-INF/views/template/template1.jsp"></jsp:include>

<h1>Registrar</h1>
<form class="form-horizontal" action="/curso-mvc/funcionario/salvar"
	method="post">

	<div class="form-group">
		<label for="nome" class="col-sm-2 control-label">Nome do Funcionario</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="nome">
		</div>
	</div>

	<div class="form-group">
		<label for="cpf" class="col-sm-2 control-label">CPF</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" name="cpf">
		</div>
	</div>


	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-default">Salvar</button>
		</div>
	</div>
</form>

<jsp:include page="/WEB-INF/views/template/template2.jsp"></jsp:include>
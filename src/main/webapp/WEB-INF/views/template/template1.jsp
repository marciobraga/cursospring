<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="springForm"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>

<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<style type="text/css">
@media ( min-width : 768px) {
	.container {
		max-width: 730px;
	}
}

.container-narrow>hr {
	margin: 30px 0;
}
</style>
</head>
<body>
	<div class="navbar-wrapper">
		<div class="container">

			<nav class="navbar navbar-inverse navbar-static-top">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed"
							data-toggle="collapse" data-target="#navbar"
							aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">Sistema de Fazendas</a>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown" role="button" aria-haspopup="true"
								aria-expanded="false">Cadastro <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="/curso-mvc/fazenda/listar/0">Fazendas</a></li>
									<li><a href="/curso-mvc/funcionario/listar">Funcionários</a></li>
									<li><a href="/curso-mvc/produto/listar">Produtos</a></li>
									<li><a href="/curso-mvc/animal/listar">Animais</a></li>
									<li><a href="/curso-mvc/lote/listar">Lotes</a></li>
									<li><a href="/curso-mvc/pasto/listar">Pastos</a></li>
								</ul></li>
							<security:authorize access="hasRole('ADMIN')">
								<li>ADMIN</li>
							</security:authorize>
							<security:authorize access="isAuthenticated()">

								<li class="dropdown"><a href="#" class="dropdown-toggle"
									data-toggle="dropdown" role="button" aria-haspopup="true"
									aria-expanded="false"> <security:authentication
											property="principal.username" /> <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><c:url value="/logout" var="logout"></c:url> <springForm:form
												action="${logout }">
												<button class="btn btn-default" type="submit">Sair</button>
											</springForm:form></li>
									</ul></li>
							</security:authorize>
						</ul>
					</div>
				</div>
			</nav>

		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
package br.com.muiraquitec.mvc.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
//import javax.persistence.OneToMany;
//import java.util.List;

@Entity
public class Animal {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String nome;
	private String genero;
	private String idade;
	private Double peso;
	
	
//	@MANYTOONE
//	@JOINCOLUMN(NAME="LOTE_ID")
//	PRIVATE LOTE LOTE;
//	
//	@ONETOMANY(MAPPEDBY="ANIMAL")
//	PRIVATE LIST<ANIMALRACA> ANIMALRACA;
	
	public String getIdade() {
		return idade;
	}

	public void setIdade(String idade) {
		this.idade = idade;
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	@ManyToOne
	@JoinColumn(name="fazenda_id")
	private Fazenda fazenda;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	
	public Fazenda getFazenda() {
		return fazenda;
	}

	public void setFazenda(Fazenda fazenda) {
		this.fazenda = fazenda;
	}
	
	

}

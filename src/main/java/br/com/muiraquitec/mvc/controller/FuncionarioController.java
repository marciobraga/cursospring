package br.com.muiraquitec.mvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.muiraquitec.mvc.dao.FuncionarioDAO;
import br.com.muiraquitec.mvc.model.Funcionario;

@Controller
@Transactional
public class FuncionarioController {

	@Autowired
	private FuncionarioDAO funcionarioDAO;

	@RequestMapping("/funcionario/cadastrar")
	public String cadastrar(Model model) {
		return "/funcionario/cadastrar";
	}

	@RequestMapping("/funcionario/editar/{id}")
	public String editar(@PathVariable Long id, Model model) {
		Funcionario funcionario = funcionarioDAO.selecionar(id);
		model.addAttribute("funcionario", funcionario);
		return "/funcionario/editar";
	}
	
	@RequestMapping("/funcionario/apagar/{id}")
	public String apagar(@PathVariable Long id, Model model) {
		Funcionario funcionario = funcionarioDAO.selecionar(id);
		funcionarioDAO.remover(funcionario);
		return "redirect:/funcionario/listar";
	}

	@RequestMapping("/funcionario/salvar")
	public String salvar(Funcionario funcionario) {
		funcionarioDAO.salvar(funcionario);
		return "redirect:/funcionario/listar";
	}
	
	@RequestMapping("/funcionario/atualizar")
	public String atualizar(Funcionario funcionario) {
		funcionarioDAO.atualizar(funcionario);
		return "redirect:/funcionario/listar";
	}

	@RequestMapping("/funcionario/listar")
	public String listar(Model model) {
		List<Funcionario> funcionarios = funcionarioDAO.listar();
		model.addAttribute("funcionarios", funcionarios);
		return "/funcionario/listar";
	}

}

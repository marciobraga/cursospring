package br.com.muiraquitec.mvc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.muiraquitec.mvc.dao.FazendaDAO;
import br.com.muiraquitec.mvc.model.Fazenda;

@Controller
@Transactional
public class FazendaController {

	@Autowired
	private FazendaDAO fazendaDAO;

	@RequestMapping("/fazenda/cadastrar")
	public String cadastrar(Model model) {
		model.addAttribute("fazenda", new Fazenda());
		return "/fazenda/cadastrar";
	}

	@RequestMapping("/fazenda/editar/{id}")
	public String editar(@PathVariable Long id, Model model) {
		Fazenda fazenda = fazendaDAO.selecionar(id);
		model.addAttribute("fazenda", fazenda);
		return "/fazenda/editar";
	}

	@RequestMapping("/fazenda/apagar/{id}")
	public String apagar(@PathVariable Long id, Model model) {
		Fazenda fazenda = fazendaDAO.selecionar(id);
		fazendaDAO.remover(fazenda);
		return "redirect:/fazenda/listar";
	}

	@RequestMapping("/fazenda/salvar")
	public String salvar(@Valid Fazenda fazenda, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			return "/fazenda/cadastrar";
		}
		fazendaDAO.salvar(fazenda);
		return "redirect:/fazenda/listar";
	}

	@RequestMapping("/fazenda/pesquisar")
	public String pesquisar(@RequestParam(value = "filter") String filter, Model model) {
		List<Fazenda> fazendas = fazendaDAO.buscarProprietario(filter);
		model.addAttribute("fazendas", fazendas);
		return "/fazenda/listar";
	}

	@RequestMapping("/fazenda/atualizar")
	public String atualizar(@Valid Fazenda fazenda, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			return "/fazenda/editar";
		}
		fazendaDAO.atualizar(fazenda);
		return "redirect:/fazenda/listar";
	}

	@RequestMapping("/fazenda/listar")
	public String listar(@RequestParam(value = "pagina", defaultValue = "0") Integer pagina, Model model) {
		List<Fazenda> fazendas = fazendaDAO.paginar(pagina, 5);
		Long total = fazendaDAO.quantidade();
		model.addAttribute("fazendas", fazendas);
		model.addAttribute("total", total);
		return "/fazenda/listar";
	}

}

package br.com.muiraquitec.mvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;

import br.com.muiraquitec.mvc.dao.FazendaDAO;
import br.com.muiraquitec.mvc.dao.LoteDAO;
import br.com.muiraquitec.mvc.model.Fazenda;
import br.com.muiraquitec.mvc.model.Lote;

@Controller
@Transactional
public class LoteController {

	@Autowired
	private FazendaDAO fazendaDAO;
	
	@Autowired
	private LoteDAO loteDAO;

	@RequestMapping("/lote/cadastrar")
	public String cadastrar(Model model) {
		List<Fazenda> fazendas = fazendaDAO.listar();
		model.addAttribute("fazendas", fazendas);
		model.addAttribute("lote", new Lote());
		return "/lote/cadastrar";
	}

	@RequestMapping("/lote/editar/{id}")
	public String editar(@PathVariable Long id, Model model) {
		Lote lote = loteDAO.selecionar(id);
		model.addAttribute("lote", lote);
		return "/lote/editar";
	}
	
	@RequestMapping("/lote/apagar/{id}")
	public String apagar(@PathVariable Long id, Model model) {
		Lote lote = loteDAO.selecionar(id);
		loteDAO.remover(lote);
		return "redirect:/lote/listar";
	}

	@RequestMapping("/lote/salvar")
	public String salvar(@Valid Fazenda fazenda,BindingResult bindingResult, Lote lote) {
		loteDAO.salvar(lote);
		return "redirect:/lote/listar";
	}
	
	@RequestMapping("/lote/atualizar")
	public String atualizar(@Valid Fazenda fazenda, BindingResult bindingResult, Lote lote) {
		loteDAO.atualizar(lote);
		return "redirect:/lote/listar";
	}

	@RequestMapping("/lote/listar")
	public String listar(Model model) {
		List<Lote> lotes = loteDAO.listar();
		model.addAttribute("lotes", lotes);
		return "/lote/listar";
	}

}

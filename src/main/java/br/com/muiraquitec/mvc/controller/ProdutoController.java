package br.com.muiraquitec.mvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.muiraquitec.mvc.dao.ProdutoDAO;
import br.com.muiraquitec.mvc.model.Produto;

@Controller
@Transactional
public class ProdutoController {
	
	@Autowired
	private ProdutoDAO produtoDAO;

	@RequestMapping("/produto/cadastrar")
	public String cadastrar(Model model) {
		return "/produto/cadastrar";
	}

	@RequestMapping("/produto/editar/{id}")
	public String editar(@PathVariable Long id, Model model) {
		Produto produto = produtoDAO.selecionar(id);
		model.addAttribute("produto", produto);
		return "/produto/editar";
	}
	
	@RequestMapping("/produto/apagar/{id}")
	public String apagar(@PathVariable Long id, Model model) {
		Produto produto = produtoDAO.selecionar(id);
		produtoDAO.remover(produto);
		return "redirect:/produto/listar";
	}

	@RequestMapping("/produto/salvar")
	public String salvar(Produto produto) {
		produtoDAO.salvar(produto);
		return "redirect:/produto/listar";
	}
	
	@RequestMapping("/produto/atualizar")
	public String atualizar(Produto produto) {
		produtoDAO.atualizar(produto);
		return "redirect:/produto/listar";
	}

	@RequestMapping("/produto/listar")
	public String listar(Model model) {
		List<Produto> produtos = produtoDAO.listar();
		model.addAttribute("produtos", produtos);
		return "/produto/listar";
	}

}

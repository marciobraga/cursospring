package br.com.muiraquitec.mvc.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.muiraquitec.mvc.dao.GenericDAO;

public class GenericDAOImpl<PK, T> implements GenericDAO<PK, T> {

	@PersistenceContext
	protected EntityManager entityManager;

	private Class<T> clazz;

	public GenericDAOImpl(Class<T> clazz) {
		this.clazz = clazz;
	}

	public void salvar(T object) {
		entityManager.persist(object);
	}

	public void atualizar(T object) {
		entityManager.merge(object);
	}

	public void remover(T object) {
		entityManager.remove(object);
	}

	public List<T> listar() {
		Query query = entityManager.createQuery("from " + clazz.getName());
		return query.getResultList();
	}

	public List<T> paginar(int pagina, int tamanho) {
		Query query = entityManager.createQuery("from " + clazz.getName());
		query.setFirstResult(pagina * tamanho);
		query.setMaxResults(tamanho);
		return query.getResultList();
	}
	
	public Long quantidade() {
		Query query = entityManager.createQuery("select count(1) from " + clazz.getName());		
		return (Long) query.getSingleResult();
	}

	public T selecionar(PK pk) {
		T object = (T) entityManager.find(clazz, pk);
		return object;
	}

}

package br.com.muiraquitec.mvc.dao;

import java.util.List;

public interface GenericDAO<PK, T> {
	
	public void salvar(T object);

	public void atualizar(T object);

	public void remover(T object);

	public List<T> listar();
	
	public List<T> paginar(int pagina, int tamanho);
	
	public Long quantidade();

	public T selecionar(PK pk);
}

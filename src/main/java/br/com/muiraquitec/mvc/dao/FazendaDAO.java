package br.com.muiraquitec.mvc.dao;

import java.util.List;

import br.com.muiraquitec.mvc.model.Fazenda;
public interface FazendaDAO extends GenericDAO<Long, Fazenda>{

	public List<Fazenda> buscarProprietario(String nome);
	
}

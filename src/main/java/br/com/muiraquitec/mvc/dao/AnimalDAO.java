package br.com.muiraquitec.mvc.dao;

import br.com.muiraquitec.mvc.model.Animal;

public interface AnimalDAO extends GenericDAO<Long, Animal>{

}
package br.com.muiraquitec.mvc.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.muiraquitec.mvc.dao.PastoDAO;
import br.com.muiraquitec.mvc.model.Pasto;

@Repository
public class PastoDAOImpl extends GenericDAOImpl<Long, Pasto> implements PastoDAO {

	public PastoDAOImpl() {
		super(Pasto.class);
	}

	@PersistenceContext
	private EntityManager entityManager;

}

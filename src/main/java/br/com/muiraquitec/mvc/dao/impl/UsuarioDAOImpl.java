package br.com.muiraquitec.mvc.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.muiraquitec.mvc.dao.AnimalDAO;
import br.com.muiraquitec.mvc.dao.UsuarioDAO;
import br.com.muiraquitec.mvc.model.Animal;
import br.com.muiraquitec.mvc.model.Usuario;

@Repository
public class UsuarioDAOImpl extends GenericDAOImpl<String, Usuario> implements UsuarioDAO {

	public UsuarioDAOImpl() {
		super(Usuario.class);
	}

	@PersistenceContext
	private EntityManager entityManager;

	public Usuario buscarPorEmail(String email) {
		Query query = entityManager.createQuery("from Usuario where email = :email");
		query.setParameter("email", email);
		return (Usuario) query.getSingleResult();
	}

}